package fr.lorenzocacciato.hiaqueue.utils

interface HIALoadServiceListener {

    fun onSuccessLoading()

    fun onFailedLoading()

}
