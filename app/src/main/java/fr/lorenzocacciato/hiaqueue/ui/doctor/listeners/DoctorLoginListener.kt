package fr.lorenzocacciato.hiaqueue.ui.doctor.listeners

interface DoctorLoginListener {

    fun onLoginSuccess()

    fun onLoginFailed()

}