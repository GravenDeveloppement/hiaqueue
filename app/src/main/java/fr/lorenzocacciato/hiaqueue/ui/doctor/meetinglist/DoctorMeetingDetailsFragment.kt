
package fr.lorenzocacciato.hiaqueue.ui.doctor.meetinglist

import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.lorenzocacciato.hiaqueue.R
import fr.lorenzocacciato.hiaqueue.fragments.MeetingDialogFragment
import fr.lorenzocacciato.hiaqueue.ui.doctor.meetinglist.adapters.DoctorHoursCalendarAdapter
import fr.lorenzocacciato.hiaqueue.ui.doctor.meetinglist.adapters.utils.ItemOffsetDecoration
import fr.lorenzocacciato.hiaqueue.extensions.formatToViewDateDefaults
import fr.lorenzocacciato.hiaqueue.model.DoctorReviewRequest
import fr.lorenzocacciato.hiaqueue.persistence.HIAQueueServiceManager
import fr.lorenzocacciato.hiaqueue.ui.doctor.listeners.DoctorReviewApproveListener
import fr.lorenzocacciato.hiaqueue.wheelpicker.aigestudio.widgets.WheelDatePicker
import java.util.*



class DoctorMeetingDetailsFragment : MeetingDialogFragment(),
    WheelDatePicker.OnDateSelectedListener,
    DoctorHoursCalendarAdapter.OnHourSelectListener, DoctorReviewApproveListener {

    private val servicesManager: HIAQueueServiceManager by lazy { HIAQueueServiceManager.getInstance() }

    private val hourDoctorAdapter by lazy { DoctorHoursCalendarAdapter(context!!, resources.getIntArray(R.array.hours_of_services)) }
    private val wheelDatePickerView by lazy { view!!.findViewById<WheelDatePicker>(R.id.doctor_request_details_section_date_picker) }
    private val hoursCalendarRecycler by lazy { view!!.findViewById<RecyclerView>(R.id.doctor_request_details_section_period_calendar) }

    private val confirmButton by lazy { view!!.findViewById<Button>(R.id.doctor_request_details_confirm_button) }
    private val cancelButton by lazy { view!!.findViewById<Button>(R.id.doctor_request_details_cancel_button) }

    private val meetingRequest by lazy { arguments!!.getParcelable<DoctorReviewRequest>("meetingRequest") }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_doctor_meeting_details, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /* init default dialog size to full screen mode */
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /* init meeting request data to views */
        initView(R.id.doctor_request_details_onboard_lastname_value, meetingRequest.lastname.toUpperCase())
        initView(R.id.doctor_request_details_onboard_surname_value, meetingRequest.surname)
        initView(R.id.doctor_request_details_onboard_consultation_type_value, meetingRequest.consultationTypeName)

        /* init meeting request to recycler */
        hourDoctorAdapter.setCurrentMeetingRequest(meetingRequest)
        hoursCalendarRecycler.adapter = hourDoctorAdapter

        /* init adapter to hours calendar recycler view */
        hoursCalendarRecycler.layoutManager = GridLayoutManager(context, 4)
        hoursCalendarRecycler.addItemDecoration(ItemOffsetDecoration(context!!, R.dimen.dp2))

        /* init listeners */
        hourDoctorAdapter.setOnHourSelectListener(this)
        wheelDatePickerView.setOnDateSelectedListener(this)

        /* init confirm button click listener */
        confirmButton.setOnClickListener {

            /* update calendar with hour */
            val calendar = Calendar.getInstance()
            calendar.time = wheelDatePickerView.currentDate
            calendar.set(Calendar.HOUR_OF_DAY, hourDoctorAdapter.getSelectedHours())

            /* approve request */
            servicesManager.acceptMeetingRequest(meetingRequest.id, calendar.time, this)

        }

        /* init cancel button click listener */
        cancelButton.setOnClickListener { dismiss()}

        /* init and update confirm button */
        updateConfirmButton()

    }

    override fun onDateSelected(picker: WheelDatePicker?, date: Date?) {  updateConfirmButton() }

    override fun onHourSelected(hour: Int) { updateConfirmButton() }

    override fun onApprove() {

        /* display notification result */
        Toast.makeText(context, "onSent() ", Toast.LENGTH_SHORT).show()

        /* back to meeting requests list */
        dismiss()

    }

    private fun updateConfirmButton() {

        /* update the confirm button date value */
        confirmButton.text = resources.getString(R.string.doctor_request_details_confirm_button_title, wheelDatePickerView.currentDate.formatToViewDateDefaults(), hourDoctorAdapter.getSelectedHours())

    }

}
