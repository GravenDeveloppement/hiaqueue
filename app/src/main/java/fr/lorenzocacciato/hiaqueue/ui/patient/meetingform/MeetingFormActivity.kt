package fr.lorenzocacciato.hiaqueue.ui.patient.meetingform

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import fr.lorenzocacciato.hiaqueue.R
import fr.lorenzocacciato.hiaqueue.extensions.afterTextChanged
import fr.lorenzocacciato.hiaqueue.extensions.isValidEmail
import fr.lorenzocacciato.hiaqueue.model.data.MeetingFormData
import fr.lorenzocacciato.hiaqueue.model.body.patient.AccountSession
import fr.lorenzocacciato.hiaqueue.persistence.HIAQueueServiceManager
import fr.lorenzocacciato.hiaqueue.ui.patient.listeners.AccountSessionListener
import fr.lorenzocacciato.hiaqueue.wheelpicker.aigestudio.WheelPicker

class MeetingFormActivity : AppCompatActivity(), WheelPicker.OnItemSelectedListener,
    AccountSessionListener {

    private val servicesManager: HIAQueueServiceManager by lazy { HIAQueueServiceManager.getInstance() }

    companion object {
        const val TAG = "MeetingFormActivity.TAG"
    }

    /** auto lazy init input form views */
    private val wheelPickerView: WheelPicker by lazy { findViewById<WheelPicker>(R.id.meeting_form_consultation_type_list) }
    private val editTextFormSurName: EditText by lazy { findViewById<EditText>(R.id.meeting_form_surname_input) }
    private val editTextFormLastName: EditText by lazy { findViewById<EditText>(R.id.meeting_form_lastname_input) }
    private val editTextFormEmail: EditText by lazy { findViewById<EditText>(R.id.meeting_form_email_input) }
    private val estimationTimeView: TextView by lazy { findViewById<TextView>(R.id.meeting_form_consultation_estimation_time) }
    private val sendButtonView: Button by lazy { findViewById<Button>(R.id.meeting_form_send_button) }

    /** auto lazy init meeting form data */
    private val meetingFormData: MeetingFormData by lazy { MeetingFormData() }
    private val context: Context by lazy { this }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        /* init meeting form form layout */
        setContentView(R.layout.activity_meeting_request_form)

        /* load wheel picker view data */
        loadWheelPickerViewData()

        /* assign to an onItemClickListener to perform click event */
        wheelPickerView.setOnItemSelectedListener(this)

        /* init email validation input */
        initForm()

    }

    private fun initForm() {

        editTextFormLastName.afterTextChanged {
            meetingFormData.lastname = it
            checkButton()
        }

        editTextFormSurName.afterTextChanged {
            meetingFormData.surname = it
            checkButton()
        }

        editTextFormEmail.afterTextChanged {
            meetingFormData.email = it

            if(!it.isValidEmail())
            {
                meetingFormData.email = ""
            }

            checkButton()
        }
    }

    private fun checkButton() {
        if(meetingFormData.isCompleted()){
            sendButtonView.isEnabled = true
            sendButtonView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
        } else{
            sendButtonView.isEnabled = false
            sendButtonView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGray))
        }
    }

    private fun loadWheelPickerViewData() {

        /* assign list as wheel picker data */
        wheelPickerView.data = servicesManager.getConsultationTypesName()

        /* turn wheel picker as visible */
        wheelPickerView.visibility = View.VISIBLE

        /* init a default consultation type list */
        meetingFormData.consultationType = servicesManager.getConsultationTypeById(0)

        /* init default estimation time */
        estimationTimeView.text = resources.getString(R.string.meeting_form_time_estimation_title, meetingFormData.consultationType!!.estimationTime)

    }

    override fun onItemSelected(picker: WheelPicker, data: Any, position: Int) {
        /* get consultation type object from item position */
        val consultationType = servicesManager.getConsultationTypeById(position)

        /* update temp meeting form data object */
        meetingFormData.consultationType = consultationType

        /* update consultation estimation time */
        estimationTimeView.text = resources.getString(R.string.meeting_form_time_estimation_title, meetingFormData.consultationType!!.estimationTime)

    }

    fun onSubmitForm(view: View) {

        /* creating meeting form details fragment instance */
        val newFragment = MeetingFormDetailsFragment()
        val bundle = Bundle()
        bundle.putParcelable("meetingForm", meetingFormData)
        newFragment.arguments = bundle

        /* load patient account */
        servicesManager.loadPatientAccount(meetingFormData, this)

        /* display and build current dialog fragment */
        newFragment.show(supportFragmentManager, "dialog")

    }

    override fun onLoad(accountSession: AccountSession) {
        Toast.makeText(context, "onAccountSessionLoaded() ", Toast.LENGTH_SHORT).show()
    }


}

