package fr.lorenzocacciato.hiaqueue.ui.doctor.meetinglist.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import fr.lorenzocacciato.hiaqueue.R
import fr.lorenzocacciato.hiaqueue.model.DoctorReviewRequest

class DoctorHoursCalendarAdapter(
    private val context: Context,
    private var hoursOfService: IntArray
) : RecyclerView.Adapter<DoctorHoursCalendarAdapter.ViewHolder>() {

    private var hourSelectListener: OnHourSelectListener? = null
    private var selectedHours = hoursOfService[0]
    private var lastSelectedItem: TextView? = null
    private var meetingRequest: DoctorReviewRequest = DoctorReviewRequest()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_calendar_hours, parent, false)
        return ViewHolder(context, view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        /** init item hour view */
        holder.itemHourView.text = context.resources.getString(R.string.doctor_request_details_section_period_item, hoursOfService[position])

        /** add star icons period preference */
        if (isInPeriod(meetingRequest.preferences, hoursOfService[position]))
        {
            holder.itemStarIconView.visibility = View.VISIBLE
        }

        /** assign on select hour item listener */
        holder.itemLayout.setOnClickListener { selectHourItem(holder, position) }

    }

    private fun selectHourItem(holder: ViewHolder, position: Int) {

        /** if has previous selected item */
        if(lastSelectedItem != null) {
            /** reset color to previous selected item */
            lastSelectedItem!!.setBackgroundColor(ContextCompat.getColor(context, R.color.colorDarkGray))
        }

        /** assign new selected item */
        lastSelectedItem = holder.itemHourView
        selectedHours = hoursOfService[position]

        /** update background item color */
        lastSelectedItem!!.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPurple))

        /** hour select callback */
        hourSelectListener?.onHourSelected(selectedHours)

    }

    override fun getItemCount(): Int {
        return hoursOfService.size
    }

    private fun isInPeriod(periodPreference: Int, hour: Int): Boolean {
        return if(periodPreference == 1) (hour <= 12) else (hour > 12)
    }

    fun setOnHourSelectListener(hourSelectListener: DoctorHoursCalendarAdapter.OnHourSelectListener) {
        this.hourSelectListener = hourSelectListener
    }

    fun setCurrentMeetingRequest(meetingReviewRequest: DoctorReviewRequest) {
        this.meetingRequest = meetingReviewRequest
    }

    fun getSelectedHours(): Int {
        return selectedHours
    }

    class ViewHolder(context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemLayout = itemView
        var itemHourView = itemLayout.findViewById<TextView>(R.id.item_calendar_hours)
        var itemStarIconView = itemLayout.findViewById<ImageView>(R.id.item_calendar_star)

    }

    interface OnHourSelectListener {
        fun onHourSelected(hour: Int)
    }


}