package fr.lorenzocacciato.hiaqueue.ui.doctor.listeners

import fr.lorenzocacciato.hiaqueue.model.DoctorReviewRequest

interface DoctorReviewRequestListListener {

    fun onGet(meetingReviewRequest: Array<DoctorReviewRequest>)

}