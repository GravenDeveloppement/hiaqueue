package fr.lorenzocacciato.hiaqueue.ui.doctor.meetinglist

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.lorenzocacciato.hiaqueue.R
import fr.lorenzocacciato.hiaqueue.model.DoctorReviewRequest
import fr.lorenzocacciato.hiaqueue.model.body.doctor.DoctorLoginSession
import fr.lorenzocacciato.hiaqueue.persistence.HIAQueueServiceManager
import fr.lorenzocacciato.hiaqueue.ui.doctor.listeners.DoctorReviewRequestListListener
import fr.lorenzocacciato.hiaqueue.ui.doctor.meetinglist.adapters.DoctorMeetingRecyclerViewAdapter

class DoctorMeetingListActivity : AppCompatActivity() {

    private val servicesManager: HIAQueueServiceManager by lazy { HIAQueueServiceManager.getInstance() }
    private val doctorSession: DoctorLoginSession by lazy { servicesManager.getDoctorSession() }

    private val meetingRecyclerView: RecyclerView by lazy { findViewById<RecyclerView>(R.id.meeting_recycler_view) }
    private val onBoardingDoctorNameView: TextView by lazy { findViewById<TextView>(R.id.doctor_request_onboard_title) }

    private val context: Context by lazy{ this }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_meeting_list)

        // init on boarding doctor name view
        updateDoctorName()

        // load meeting requests
        loadMeetingRequests()
    }

    private fun updateDoctorName() {
        onBoardingDoctorNameView.text = resources.getString(R.string.doctor_request_recycler_onboard_title, doctorSession.lastname)
    }

    private fun loadMeetingRequests() {
        servicesManager.getMeetingRequests(object: DoctorReviewRequestListListener {

            override fun onGet(meetingReviewRequests: Array<DoctorReviewRequest>) {

                var meetingRequestList = arrayListOf<DoctorReviewRequest>()

                meetingReviewRequests.forEach {
                    meetingRequestList.add(it)
                }

                // assign layout manager
                meetingRecyclerView.layoutManager = LinearLayoutManager(context)

                // init and assign adapter to recycler view
                meetingRecyclerView.adapter =
                        DoctorMeetingRecyclerViewAdapter(
                            context,
                            supportFragmentManager,
                            meetingRequestList
                        )
            }

        })
    }

    override fun onBackPressed() { finish() }

}
