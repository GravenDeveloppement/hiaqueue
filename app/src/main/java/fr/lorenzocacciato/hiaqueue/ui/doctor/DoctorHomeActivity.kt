package fr.lorenzocacciato.hiaqueue.ui.doctor

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import fr.lorenzocacciato.hiaqueue.R
import fr.lorenzocacciato.hiaqueue.model.data.DoctorLogInFormData
import fr.lorenzocacciato.hiaqueue.extensions.afterTextChanged
import fr.lorenzocacciato.hiaqueue.extensions.isValidEmail
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import fr.lorenzocacciato.hiaqueue.persistence.HIAQueueServiceManager
import fr.lorenzocacciato.hiaqueue.ui.doctor.listeners.DoctorLoginListener
import fr.lorenzocacciato.hiaqueue.ui.doctor.meetinglist.DoctorMeetingListActivity


class DoctorHomeActivity : AppCompatActivity(), DoctorLoginListener {

    private val servicesManager: HIAQueueServiceManager? by lazy { HIAQueueServiceManager.getInstance() }

    private val switchModeTextView: TextView by lazy { findViewById<TextView>(R.id.home_toggle_mode_switch_button) }
    private val editTextFormEmail: EditText by lazy { findViewById<EditText>(R.id.doctor_home_email_input) }
    private val editTextFormPassword: EditText by lazy { findViewById<EditText>(R.id.doctor_home_password_input) }
    private val sendButtonView: Button by lazy { findViewById<Button>(R.id.doctor_home_sign_in_button) }

    /** auto lazy init meeting form data */
    private val doctorLogInFormData: DoctorLogInFormData by lazy { DoctorLogInFormData() }
    private val context: Context by lazy { this }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_home)

        // init password edit text input
        editTextFormPassword.transformationMethod = PasswordTransformationMethod();

        // init switch mode text view
        switchModeTextView.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        switchModeTextView.setOnClickListener { finish() }

        initForm()

    }

    fun onLogInAttempt(view: View) {
        servicesManager!!.loginAttempt(doctorLogInFormData, this)
    }

    override fun onLoginSuccess() {
        startActivity(Intent(applicationContext, DoctorMeetingListActivity::class.java))
        Toast.makeText(context, "Login good ! ", Toast.LENGTH_SHORT).show()
    }

    override fun onLoginFailed() {
        Toast.makeText(context, "Login failed ! ", Toast.LENGTH_SHORT).show()
    }

    private fun checkButton() {
        if(doctorLogInFormData.isCompleted()){
            sendButtonView.isEnabled = true
            sendButtonView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorSecondary))
        } else{
            sendButtonView.isEnabled = false
            sendButtonView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGray))
        }
    }


    private fun initForm() {

        editTextFormEmail.afterTextChanged {
            doctorLogInFormData.email = it

            if(!it.isValidEmail())
            {
                doctorLogInFormData.email = ""
            }

            checkButton()
        }


        editTextFormPassword.afterTextChanged {
            doctorLogInFormData.password = it
            checkButton()
        }

    }

    override fun onBackPressed() { finish() }

}
