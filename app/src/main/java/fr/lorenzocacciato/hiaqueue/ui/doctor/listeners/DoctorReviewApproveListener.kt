package fr.lorenzocacciato.hiaqueue.ui.doctor.listeners

interface DoctorReviewApproveListener {

    fun onApprove()

}