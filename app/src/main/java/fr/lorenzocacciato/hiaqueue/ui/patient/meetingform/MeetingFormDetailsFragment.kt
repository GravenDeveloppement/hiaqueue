package fr.lorenzocacciato.hiaqueue.ui.patient.meetingform

import android.content.Intent
import android.os.Bundle
import androidx.annotation.Nullable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import fr.lorenzocacciato.hiaqueue.R
import fr.lorenzocacciato.hiaqueue.fragments.MeetingDialogFragment
import fr.lorenzocacciato.hiaqueue.model.data.MeetingFormData
import fr.lorenzocacciato.hiaqueue.model.body.patient.RequestSession
import fr.lorenzocacciato.hiaqueue.persistence.HIAQueueServiceManager
import fr.lorenzocacciato.hiaqueue.ui.patient.HomeActivity
import fr.lorenzocacciato.hiaqueue.ui.patient.listeners.RequestSendingListener

class MeetingFormDetailsFragment : MeetingDialogFragment(), RequestSendingListener{

    private val meetingFormData: MeetingFormData by lazy { arguments!!.getParcelable<MeetingFormData>("meetingForm") }
    private val servicesManager: HIAQueueServiceManager by lazy { HIAQueueServiceManager.getInstance() }
    private val sendButtonView: Button by lazy { view!!.findViewById<Button>(R.id.meeting_form_details_send_button) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_meeting_form_details, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /** show edit text values from meeting form data temp object */
        initView(R.id.meeting_form_details_surname_value, meetingFormData.surname)
        initView(R.id.meeting_form_details_lastname_value, meetingFormData.lastname)
        initView(R.id.meeting_form_details_email_value, meetingFormData.email)
        initView(R.id.meeting_form_details_consultation_type_value, meetingFormData.consultationType!!.label)

        /** init form details click listener */
        sendButtonView.setOnClickListener { servicesManager.createMeetingRequest(meetingFormData, this) }

    }

    override fun onSending(requestSession: RequestSession) {
        Toast.makeText(context, "onRequestSending() ", Toast.LENGTH_SHORT).show()
        startActivity(Intent(context, HomeActivity::class.java))
    }

    override fun onStart() {
        super.onStart()
        val d = dialog
        if (d != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            d.window!!.setLayout(width, height)
        }
    }


}
