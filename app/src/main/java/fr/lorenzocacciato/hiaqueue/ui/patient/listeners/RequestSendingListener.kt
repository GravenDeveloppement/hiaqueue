package fr.lorenzocacciato.hiaqueue.ui.patient.listeners

import fr.lorenzocacciato.hiaqueue.model.body.patient.RequestSession

interface RequestSendingListener {

    fun onSending(requestSession: RequestSession)

}
