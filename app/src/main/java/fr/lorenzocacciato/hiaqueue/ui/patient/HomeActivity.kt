package fr.lorenzocacciato.hiaqueue.ui.patient

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import fr.lorenzocacciato.hiaqueue.R
import fr.lorenzocacciato.hiaqueue.persistence.HIAQueueServiceManager
import fr.lorenzocacciato.hiaqueue.ui.doctor.DoctorHomeActivity
import fr.lorenzocacciato.hiaqueue.ui.patient.meetingform.MeetingFormActivity
import fr.lorenzocacciato.hiaqueue.utils.HIALoadServiceListener

class HomeActivity : AppCompatActivity(), HIALoadServiceListener {


    private val servicesManager: HIAQueueServiceManager? by lazy { HIAQueueServiceManager.getInstance() }

    private val meetingButton: Button by lazy { findViewById<Button>(R.id.home_create_meeting_button) }
    private val switchModeTextView: TextView by lazy { findViewById<TextView>(R.id.home_toggle_mode_switch_button) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        /* init switch mode text view */
        switchModeTextView.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        switchModeTextView.setOnClickListener { startActivity(Intent(applicationContext, DoctorHomeActivity::class.java)) }
        meetingButton.setOnClickListener { startActivity(Intent(applicationContext, MeetingFormActivity::class.java)) }

        /* check API initiazing */
        servicesManager!!.isLoaded(this)

    }

    override fun onSuccessLoading() {
        servicesManager!!.loadConsultationTypes()
    }

    override fun onFailedLoading() {
        Toast.makeText(applicationContext, "API Error", Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() { /* cancel back button press */ }

}
