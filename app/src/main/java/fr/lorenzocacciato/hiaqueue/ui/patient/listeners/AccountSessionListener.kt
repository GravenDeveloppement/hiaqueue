package fr.lorenzocacciato.hiaqueue.ui.patient.listeners

import fr.lorenzocacciato.hiaqueue.model.body.patient.AccountSession

interface AccountSessionListener {

    fun onLoad(accountSession: AccountSession)

}
