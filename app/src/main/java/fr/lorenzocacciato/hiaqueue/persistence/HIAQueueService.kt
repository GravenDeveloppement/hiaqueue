package fr.lorenzocacciato.hiaqueue.persistence

import fr.lorenzocacciato.hiaqueue.model.DoctorReviewRequest
import fr.lorenzocacciato.hiaqueue.model.Version
import fr.lorenzocacciato.hiaqueue.model.body.patient.AccountSession
import fr.lorenzocacciato.hiaqueue.model.consultation.ConsultationType
import fr.lorenzocacciato.hiaqueue.model.consultation.MeetingRequestData
import fr.lorenzocacciato.hiaqueue.model.body.doctor.DoctorLoginSession
import fr.lorenzocacciato.hiaqueue.model.body.doctor.DoctorRequestApproval
import fr.lorenzocacciato.hiaqueue.model.body.patient.RequestSession
import retrofit2.Call
import retrofit2.http.*

interface HIAQueueService {

    @GET("/version")
    fun getVersion() : Call<Version>

    @GET("/consultationType/all")
    fun getConsultationTypes() : Call<Array<ConsultationType>>

    @POST("/patient/account")
    fun getPatientAccount(@Body meetingFormData: AccountSession) : Call<AccountSession>?

    @POST("/patient/meeting/request")
    fun createMeetingRequest(@Header("x-patient-token") token: String, @Body meetingRequestBody: RequestSession) : Call<MeetingRequestData>

    @POST("/doctor/account/login")
    fun login(@Body doctorAccountSession: DoctorLoginSession) : Call<DoctorLoginSession>

    @GET("/doctor/meeting/request/all")
    fun getMeetingRequests(@Header("x-doctor-token") token: String) : Call<Array<DoctorReviewRequest>>

    @GET("/doctor/calendar/{doctor_id}")
    fun getDoctorCalendar(@Path("doctor_id") doctorId: Int?, @Header("x-doctor-token") auth_token: String) : Call<Array<DoctorReviewRequest>>

    @POST("/doctor/meeting/accept")
    fun acceptMeeting(@Header("x-doctor-token") token: String, @Body meetingRequestApproval: DoctorRequestApproval) : Call<DoctorRequestApproval>


}

