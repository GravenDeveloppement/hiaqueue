package fr.lorenzocacciato.hiaqueue.persistence

import android.util.Log
import fr.lorenzocacciato.hiaqueue.extensions.formatToServerTime
import fr.lorenzocacciato.hiaqueue.model.body.patient.AccountPatient
import fr.lorenzocacciato.hiaqueue.model.DoctorReviewRequest
import fr.lorenzocacciato.hiaqueue.model.Version
import fr.lorenzocacciato.hiaqueue.model.body.patient.AccountSession
import fr.lorenzocacciato.hiaqueue.model.consultation.ConsultationType
import fr.lorenzocacciato.hiaqueue.model.consultation.MeetingRequestData
import fr.lorenzocacciato.hiaqueue.model.data.DoctorLogInFormData
import fr.lorenzocacciato.hiaqueue.model.data.MeetingFormData
import fr.lorenzocacciato.hiaqueue.model.body.doctor.DoctorLoginSession
import fr.lorenzocacciato.hiaqueue.model.body.doctor.DoctorRequestApproval
import fr.lorenzocacciato.hiaqueue.model.body.patient.RequestSession
import fr.lorenzocacciato.hiaqueue.ui.doctor.listeners.DoctorLoginListener
import fr.lorenzocacciato.hiaqueue.ui.doctor.listeners.DoctorReviewApproveListener
import fr.lorenzocacciato.hiaqueue.ui.doctor.listeners.DoctorReviewRequestListListener
import fr.lorenzocacciato.hiaqueue.ui.patient.listeners.AccountSessionListener
import fr.lorenzocacciato.hiaqueue.ui.patient.listeners.RequestSendingListener
import fr.lorenzocacciato.hiaqueue.utils.HIALoadServiceListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.sql.Timestamp
import java.util.*

class HIAQueueServiceManager {

    private var service: HIAQueueService? = null

    private var doctorLoginSession: DoctorLoginSession = DoctorLoginSession()
    private var consultationTypeList: MutableList<ConsultationType> = mutableListOf()

    companion object {
        const val TAG = "ServiceGenerator"
        const val END_POINT = "http://hia.graven.yt/"

        private var INSTANCE: HIAQueueServiceManager ? = null
        fun getInstance(): HIAQueueServiceManager{
            if(INSTANCE == null){
                INSTANCE = HIAQueueServiceManager()
            }
            return INSTANCE!!
        }

    }

    init {

        /* init HIAQueue API main service */
        service = Retrofit.Builder()
            .baseUrl(END_POINT)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create<HIAQueueService>(HIAQueueService::class.java)

        /* log it */
        Log.d(TAG, "HIAQueue Retrofit services loaded !")

    }

    fun isLoaded(hiaLoadServiceListener: HIALoadServiceListener) {
        service?.getVersion()!!.enqueue(object: Callback<Version> {

            override fun onResponse(call: Call<Version>, response: Response<Version>) {

                val serviceApiVersion = response.body()!!

                if (!serviceApiVersion.error)
                {
                    hiaLoadServiceListener.onSuccessLoading()
                }

                else
                {
                    hiaLoadServiceListener.onFailedLoading()
                }

            }

            override fun onFailure(call: Call<Version>, t: Throwable) {
               hiaLoadServiceListener.onFailedLoading()
            }

        })
    }

    fun loadConsultationTypes() {

        service?.getConsultationTypes()?.enqueue(object : Callback<Array<ConsultationType>> {

            override fun onResponse(call: Call<Array<ConsultationType>>, response: Response<Array<ConsultationType>>) {
                consultationTypeList = response.body()!!.toMutableList()
            }

            override fun onFailure(call: Call<Array<ConsultationType>>, t: Throwable) {}

        })

    }

    fun getMeetingRequests(doctorReviewRequestListListener: DoctorReviewRequestListListener) {

        Log.d(TAG, "token ${doctorLoginSession.token}")

        service?.getMeetingRequests(doctorLoginSession.token)?.enqueue(object : Callback<Array<DoctorReviewRequest>> {

            override fun onResponse(call: Call<Array<DoctorReviewRequest>>, response: Response<Array<DoctorReviewRequest>>) {
                doctorReviewRequestListListener.onGet(response.body()!!)
            }

            override fun onFailure(call: Call<Array<DoctorReviewRequest>>, t: Throwable) {
                Log.d(TAG, "onFailure({${t.cause}})")
            }
        })

    }

    fun getConsultationTypeById(position: Int): ConsultationType {
        return consultationTypeList[position]
    }

    fun getConsultationTypesName(): MutableList<String>? {
        val consultationTypeNames = mutableListOf<String>()
        consultationTypeList.forEach { consultationTypeNames.add(it.label) }
        return consultationTypeNames
    }

    fun loadPatientAccount(meetingFormData: MeetingFormData, accountSessionListener: AccountSessionListener) {

        // create account session body
        val body = AccountSession()
        val patient = AccountPatient()
        patient.lastname = meetingFormData.lastname
        patient.surname = meetingFormData.surname
        patient.email = meetingFormData.email
        body.patient = patient

        // get patient account from rest api and create account if not exist
        service?.getPatientAccount(body)!!.enqueue(object : Callback<AccountSession> {

            override fun onResponse(call: Call<AccountSession>, response: Response<AccountSession>) {
                Log.d(TAG, "onSuccessPatientLoaded()")
                val session = response.body()
                meetingFormData.authToken = session!!.token
                meetingFormData.patientId = session!!.patientId
                accountSessionListener.onLoad(session)
            }

            override fun onFailure(call: Call<AccountSession>, t: Throwable) {
                Log.d(TAG, "onError()")
            }

        })

    }

    fun createMeetingRequest(meetingFormData: MeetingFormData, requestSendingListener: RequestSendingListener) {

        // create meeting request body
        val meetingRequestBody = RequestSession()
        meetingRequestBody.patientId = meetingFormData.patientId
        meetingRequestBody.consultationTypeId = meetingFormData.consultationType.id
        meetingRequestBody.preferences = 1

        Log.d(TAG, "patient id ${meetingFormData.patientId}")
        Log.d(TAG, "consultationType id ${meetingFormData.consultationType.id}")

        // call rest api
        service?.createMeetingRequest(meetingFormData.authToken, meetingRequestBody)!!.enqueue(object: Callback<MeetingRequestData> {

            override fun onResponse(call: Call<MeetingRequestData>, rsp: Response<MeetingRequestData>) {
                Log.d(TAG, "onSuccessMeetingRequestSending()")
                requestSendingListener.onSending(meetingRequestBody)
            }

            override fun onFailure(call: Call<MeetingRequestData>, t: Throwable) {
                Log.d(TAG, "onFailingMeetingRequestSending()")
            }

        })

    }

    fun acceptMeetingRequest(meetingRequestId: Int, date: Date, doctorReviewApproveListener: DoctorReviewApproveListener) {

        // create meeting request approval body
        val requestApprovalBody = DoctorRequestApproval()
        requestApprovalBody.doctorId = doctorLoginSession.doctorId
        requestApprovalBody.meetingRequestId = meetingRequestId
        requestApprovalBody.date = date.formatToServerTime()

        // call rest api
        service?.acceptMeeting(doctorLoginSession.token, requestApprovalBody)!!.enqueue(object: Callback<DoctorRequestApproval> {

            override fun onResponse(call: Call<DoctorRequestApproval>, response: Response<DoctorRequestApproval>) {
                Log.d(TAG, "onSuccessRequestApproval()")
                doctorReviewApproveListener.onApprove()
            }

            override fun onFailure(call: Call<DoctorRequestApproval>, t: Throwable) { Log.d(TAG, "onFailingRequestApproval()") }

        })

    }

    fun loginAttempt(doctorLogInFormData: DoctorLogInFormData, loginListener: DoctorLoginListener) {

        // create doctor account login body
        doctorLoginSession.email = doctorLogInFormData.email
        doctorLoginSession.password = doctorLogInFormData.password

        // call rest api
        service?.login(doctorLoginSession)!!.enqueue(object: Callback<DoctorLoginSession> {

            override fun onResponse(call: Call<DoctorLoginSession>, response: Response<DoctorLoginSession>) {

                val doctorLoginResult = response.body()!!

                if (!doctorLoginResult.error)
                {
                    Log.d(TAG, "onLoginSuccess(${doctorLoginResult.doctorId})")

                    // assign it to local doctor session cache
                    doctorLoginSession = doctorLoginResult

                    // call on login success callback
                    loginListener.onLoginSuccess()
                }

                else
                {
                    Log.d(TAG, "onLoginFailed()")

                    // call on login failed callback
                    loginListener.onLoginFailed()
                }

            }

            override fun onFailure(call: Call<DoctorLoginSession>, t: Throwable) {
                Log.d(TAG, "onLoginFailed()")
            }

        })

    }

    fun getDoctorSession(): DoctorLoginSession {
        return doctorLoginSession
    }


}
